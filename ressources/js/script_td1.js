function adapterGalerie(nom) {
    for(let i = 1; i <= 6; i++) {
        const image = document.getElementById('fleur' + i);
        image.src = `img/fleurs/${nom}/${nom}${i}.jpg`
        image.title = `${nom}`
        image.alt = `${nom}${i}`
    }
    adapterTitre(nom)
}

/**
 *
 * @param {HTMLElement} im
 */
function cacher(im) {
    im.classList.remove("visible")
    im.classList.add("cachee")
}

/**
 * @param {HTMLElement} im
 */
function afficher(im){
    im.classList.remove("cachee")
    im.classList.add("visible")
}

/**
 * @param {number} n
 */
function suivant(n) {
    return n%6+1;
}

function construitInfobulle() {
    const info = document.createElement('div');
    info.innerHTML = "<p>c'est moi la bulle !</p>";
    info.id = "bulle";
    info.style.position = "fixed";
    info.style.top = "100px";
    info.style.right = "150px";
    info.style.backgroundColor = "darkblue";
    info.style.color = "white";
    info.style.padding = "10px";
    info.style.borderRadius = "10px";
    // info.style.boxShadow = "10%"
    document.body.appendChild(info);
}

function detruitInfobulle() {
    const info = document.getElementById('bulle');
    document.body.removeChild(info);
}

function changerParametres() {
    const bgActuel = document.body.style.backgroundImage
    let nvBg = "";
    do {
        nvBg = `url("img/background/bg-${Math.floor(Math.random() * 4)+1}.jpg")`;
    } while (bgActuel === nvBg)
    document.body.style.backgroundImage = nvBg;
}

function changeBanniereV1() {
    const elt = document.getElementsByClassName("visible")[0]
    cacher(elt)
    afficher(document.getElementById(suivant(Number(elt.id))))
}

function changeBanniereV2() {
    const elt = document.getElementsByClassName("visible")[0]
    elt.style.transition = "opacity 3s";
    cacher(elt)

    const eltSuiv = document.getElementById(suivant(Number(elt.id)))
    eltSuiv.style.transition = "opacity 3s";
    afficher(eltSuiv)
}

function stopperDefilement(){
    clearInterval(chb)
}

function lancerDefilement() {
    chb = setInterval(changeBanniereV2, 6000)
}

/**
 * @param {String} nom
 */
function adapterTitre(nom) {
    document.title = tabTitres[nom];
}

let chb = setInterval(changeBanniereV2, 6000)

const tabTitres = {
    'rose' : 'Galerie de roses',
    'hortensia': 'Galerie d’hortensias',
    'fruitier': 'Galerie de fruitiers',
    'autre': 'Galerie de fleurs diverses'
};
